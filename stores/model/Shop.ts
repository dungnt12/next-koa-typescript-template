import { Instance, SnapshotOut, types } from 'mobx-state-tree';

const Plan = types.enumeration('Plan', ['free', 'pro']);

export const ShopModel = types
  .model('Shop model', {
    _id: types.optional(types.string, ''),
    plan: types.optional(Plan, 'free'),
    myshopifyDomain: types.optional(types.string, ''),
  });

export interface IShopModel extends Instance<typeof ShopModel> {}
export interface IShopModelOut extends SnapshotOut<typeof ShopModel> {}
  