import React from 'react';
import { addMiddleware, Instance, types } from 'mobx-state-tree';
import { commonStore } from './common';
import { ShopModel } from './model/Shop';

export const StoreModel = types
  .model({
    shop: types.optional(ShopModel, {}),
  });

const store = StoreModel.create();

addMiddleware(store, (call, next) => {
  if (call.type === 'flow_resume_error') {
    try {
      next(call);
    } catch (err) {
      const error = err?.response?.data?.msg;
      if (error) {
        commonStore.setContent(err?.response?.data?.msg, true);
      }
    }
  }
  return next(call);
});

const StoreContext = React.createContext<IStoreModel>(store);

export { store, StoreContext };

export interface IStoreModel extends Instance<typeof StoreModel> {};
