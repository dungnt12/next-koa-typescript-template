import App, { AppProps } from 'next/app';
import React from 'react';
import { observer, Provider } from 'mobx-react';
import 'antd/dist/antd.css';
// Store
import { commonStore } from 'stores/common';
import { store } from 'stores';

class MyApp extends App<AppProps> {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <Provider store={store} commonStore={commonStore}>
        <Component {...pageProps} />
      </Provider>
    );
  }
}

export default observer(MyApp);
